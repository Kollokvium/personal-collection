import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sceneView.delegate = self
        sceneView.showsStatistics = true
        let scene = SCNScene(named: "art.scnassets/Bag.scn")!
        sceneView.scene = scene
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let configuration = ARWorldTrackingConfiguration()
        configuration.detectionObjects = ARReferenceObject.referenceObjects(inGroupNamed: "SomeObjects",
                                                                            bundle: Bundle.main)!
        
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }

    // MARK: - ARSCNViewDelegate
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
        
        if let objectAnchor = anchor as? ARObjectAnchor {
            let planeObject = SCNPlane(width: CGFloat(objectAnchor.referenceObject.extent.x * 1),
                                     height: CGFloat(objectAnchor.referenceObject.extent.y * 1))
            planeObject.cornerRadius = planeObject.width / 8
            
            let spriKitScene = SKScene(fileNamed: "MyBag")
            planeObject.firstMaterial?.diffuse.contents = spriKitScene
            planeObject.firstMaterial?.isDoubleSided = true
            planeObject.firstMaterial?.diffuse.contentsTransform = SCNMatrix4Translate(SCNMatrix4MakeScale(1, -1, 1), 0, 1, 0)
            
            let planeObjectNode = SCNNode(geometry: planeObject)
            planeObjectNode.position = SCNVector3Make(objectAnchor.referenceObject.center.x,
                                                      objectAnchor.referenceObject.center.y + 0.15,
                                                      objectAnchor.referenceObject.center.z)
            
            node.addChildNode(planeObjectNode)
        }
     
        return node
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
    }
}
